
# LinacKAPreAmpGUI
# Copyright (C) 2021  Control
#
# Author: E.Morales
# Author mail: emorales@cells.es
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys

from taurus.external.qt import QtCore, QtGui, QtWidgets, uic
from taurus.core import TaurusEventType
from taurus.qt.qtgui.display import TaurusLabel, TaurusLed


import traceback
import time

try:
    import tango as pt
except:
    import PyTango as pt


class MyTaurusLed(TaurusLed):

    def __init__(self, *args, **kwargs):

        super(MyTaurusLed, self).__init__()

    def handleEvent(self, evt_src, evt_type, evt_value):
        if evt_type in [TaurusEventType.Change]:
            super(MyTaurusLed, self).handleEvent(evt_src, evt_type, evt_value)
            self.updateWidget(evt_value)

    def updateWidget(self, evt_value):

        if evt_value.rvalue in [5]:
            self.setLedColor("green")  # Set color to Green

        elif evt_value.rvalue in [0]:
            self.setLedColor("black")  # Set color to Black

        else:
             self.setLedColor("orange")  # Set color to Orange


class MyTaurusLabel(TaurusLabel):

    def __init__(self, widget_list, TTI_list, rfout_widget, *args, **kwargs):

        super(MyTaurusLabel, self).__init__()

        self.widget_list = widget_list
        self.TTI_list = TTI_list
        self.rfout_widget = rfout_widget

    def handleEvent(self, evt_src, evt_type, evt_value):

        if evt_type in [TaurusEventType.Change]:
            
            super(MyTaurusLabel, self).handleEvent(evt_src, evt_type, evt_value)
            self.updateWidgets(evt_value)
            self.updateModels(evt_value)

    def updateWidgets(self, evt_value):

        for item in self.widget_list:
            if evt_value.rvalue in ["1:TTI_remote"]:
                item.setEnabled(True)
            else:
                item.setEnabled(False)

        for item in self.TTI_list:
            if evt_value.rvalue in ["1:TTI_remote", "3:TTI_local"]:
                item.setEnabled(True)
            else:
                item.setEnabled(False)

    def updateModels(self, evt_value):

        if evt_value.rvalue in ["1:TTI_remote", "3:TTI_local"]:
            self.rfout_widget.setModel("sys/tg_test/1/double_scalar")
        elif evt_value.rvalue in ["2:EADS_remote", "4:EADS_local"]:
            self.rfout_widget.setModel("sys/tg_test/1/long_scalar")


class LinacKAPreAmpGUI(QtWidgets.QWidget):

    _KA2_plc_device = 'li/ct/plc5'
    _KA2_reset = 'lab/21/li-01/TTI_intlk_res'

    def __init__(self, parent=None):

        super(LinacKAPreAmpGUI, self).__init__()

        uipath = os.path.join(os.path.dirname(__file__), "ui", "main.ui")
        self.ui = uic.loadUi(uipath, self)

        # GUI title:
        self.ui.setWindowTitle('Linac KA preamplifier GUI')

        # Widgets in GUI. Separated in 3 different list:
        #  - "KAX": Widgets not related with amp. gain or RF out signal
        #  - "KAX_gain": Amp. gain related widgets.
        #  - "KA2_rfout": RF out signal related widget.
        self.gui_obj = {
            "KA2": [self.tled_ka2_on, self.tvcheck_ka2_on,
                    self.tled_ka2_reset, self.pb_ka2_reset,
                    self.tlabel_ka2_status_read],
            "KA2_gain": [self.tlabel_ka2_gain_write,
                         self.tlabel_ka2_gain_read],
            "KA2_rfout": self.tlabel_ka2_rfout_read
            }

        # Values to pass TaurusValueComboBox. Values are tuples where first
        # value is the one showed at GUI (to choose), second value is the one
        # that will be writted to tango attribute:
        self.cb_values = (('TTI Remote', 'TTI_remote'),
                          ('TTI Local', 'TTI_local'),
                          ('EADS Remote', 'EADS_remote'),
                          ('EADS Local', 'EADS_local'))

        # Insert customized Taurus widgets:
        self.insertCustomWidgets()

        # Set models to Taurus widgets except Gain. Gain widget model will
        # change depending on 'KAX_PreAmp_Installed' attribute value:
        self.setTaurusModels()
        
        # Connect signals to reset TTI interlocks:
        self.pb_ka2_reset.clicked.connect(self.resetTTI)

    def insertCustomWidgets(self):
        """
        Method that insert customized taurus widgets in GUI layout.
        :return:
        """

        # Custom TaurusLabels: enable or disable widgets depending event value.
        self.tlabel_ka2_type_read = MyTaurusLabel(self.gui_obj["KA2"],
                                                  self.gui_obj["KA2_gain"],
                                                  self.gui_obj["KA2_rfout"])
        self.tlabel_ka2_type_read.setFixedWidth(120)
        self.tlabel_ka2_type_read.setFixedHeight(30)
        self.gridLayout_2.addWidget(self.tlabel_ka2_type_read, 0, 2)

        # Custom TaurusLed: Change color depending on attribute value.
        self.tled_ka2_status = MyTaurusLed()
        self.tled_ka2_status.setAlignment(QtCore.Qt.AlignRight)
        self.gridLayout_2.addWidget(self.tled_ka2_status, 2, 0)
        

    def resetTTI(self):
        """
        Method to reset TTI interlocks.
        :return:
        """

        try:
            att = pt.AttributeProxy(self._KA2_reset)
            print("Reset ON!!!")
            att.write(True)
            time.sleep(2)
            att.write(False)
            print("Reset OFF!!")
        
        except Exception as e:
            print("{} \n {}".format(e, traceback.format_exc()))     
    
    def setTaurusModels(self):
        """
        Method to set Taurus models in every widget.
        :return:
        """
        
        try:
            self.tled_ka2_on.setModel('lab/21/li-01/TTI_ONC')
            self.tvcheck_ka2_on.setModel('lab/21/li-01/TTI_ONC')
            
            self.tled_ka2_reset.setModel('lab/21/li-01/TTI_intlk_res')

            self.tlabel_ka2_type_write.\
                setModel('lab/21/li-01/KA2_PreAmp_Installed_active')
            self.tlabel_ka2_type_write.addValueNames(self.cb_values)
            self.tlabel_ka2_type_read.\
                setModel('lab/21/li-01/KA2_PreAmp_Installed_meaning')

            self.tlabel_ka2_status_read.setModel('lab/21/li-01/TTI_st_Status')
            self.tled_ka2_status.setModel('lab/21/li-01/TTI_st')

            self.tlabel_ka2_gain_write.\
                setModel('lab/21/li-01/Amp_gain_setpoint')
            self.tlabel_ka2_gain_read.\
                setModel('lab/21/li-01/Amp_gain')
        
        except Exception as e:
            print("Problems in setTaurusModels: {}".format(e))


def main():
    app = QtGui.QApplication(sys.argv)
    GUI = LinacKAPreAmpGUI()
    GUI.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
